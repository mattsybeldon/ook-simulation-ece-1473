function [pulse, time] = RCROPulseGenerator(Tb, kT, samplesPerBit, r)

R = 1/Tb;

time = -kT*Tb: (Tb/samplesPerBit): Tb*(kT - (1/samplesPerBit));

pulse = (sin(pi*R*time*(1-r)) + 4*R*r*time.*cos(pi*R*time*(1+r)))./(pi*R*time.*(1-(4*R*r*time).^2));

for i = 1:length(time)
    if time(i) == 0
        pulse(i) = 1 - r + (4*r)/pi;
    elseif time(i) == Tb/(4*r)
        pulse(i) = (r/sqrt(2))*((1+2/pi)*sin(pi/(4*r)) + (1-2/pi)*cos(pi/(4*r)));
    elseif time(i) == -Tb/(4*r)
        pulse(i) = (r/sqrt(2))*((1+2/pi)*sin(pi/(4*r)) + (1-2/pi)*cos(pi/(4*r)));
    end
end