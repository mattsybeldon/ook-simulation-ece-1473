function bits = polarBitRNG(numBits, polarBits)

bits = round(rand(1, numBits));

if polarBits == 1
for i = 1 : length(bits)
    if bits(i) == 0
        bits(i) = -1;
    end
end
end