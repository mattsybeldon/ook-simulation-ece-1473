clear
rng('shuffle')

Tb = 1/10;
kT = 5;
samplesPerBit = 128;
r = .5;
numBits = 100;
polarBits = 0;
fc = 100;
amplitudePulse = 1;
amplitudeCarrier = 1;

minDB = -1000;
maxDB = -1000;
noiseStepDB = 1;
numSteps= (maxDB - minDB)/noiseStepDB + 1;

numTrials = 10;


errorRateTrial = [];

[rcroPulse, timePulse] = RCROPulseGenerator(Tb, kT, samplesPerBit, r);
rcroPulse = amplitudePulse * rcroPulse;
Eb = sum((Tb/samplesPerBit)*(rcroPulse.^2));



for k = 1:numTrials
    
errorRates = [];

for j = 1:numSteps

noiseAmplitude = (Eb/(2*(10^((minDB + ((j-1)*noiseStepDB))/10))))^.5

bits = polarBitRNG(numBits, polarBits);





rcroMessage = zeros(1, numBits*samplesPerBit + 2*(length(rcroPulse)-samplesPerBit));
index = 1;

for i = 1:numBits
    rcroMessage(index : index + length(rcroPulse) - 1) = ...
        rcroMessage(index : index + length(rcroPulse) - 1) + bits(i)*rcroPulse;
    index = index + samplesPerBit;
end

% endofMessage = find(rcroMessage,1,'last')
% if isempty(endofMessage) == 1
%     return
% end
% rcroMessage = rcroMessage(1:find(rcroMessage,1,'last'));

carrier = amplitudeCarrier*cos(2*pi*fc*(0:length(rcroMessage)-1)*(Tb/samplesPerBit));

basebandMessage = rcroMessage .* carrier;

tBaseband = -kT*Tb : (Tb/samplesPerBit): numBits*Tb*(kT - (1/samplesPerBit));
tBaseband = tBaseband(1:length(basebandMessage));

% fig1 = figure();
% plot(tBaseband, basebandMessage)
% hold on
% scatter(0:19, bits)
% xlabel('Time (sec)')
% ylabel('Signal (V)')
% title('Baseband Message')

noisyMessage = basebandMessage + (noiseAmplitude)*randn(1, length(basebandMessage));

demodMessage = amdemod(noisyMessage,fc,samplesPerBit/Tb);

filteredMessage = conv(demodMessage, rcroPulse);

bitMarkersFiltered = zeros(1, length(filteredMessage));




tFiltered =  -length(rcroPulse)/samplesPerBit: (Tb/samplesPerBit) : (length(filteredMessage) - length(rcroPulse) - 1)/samplesPerBit;

estimatedBits = zeros(1, numBits);
index = length(rcroPulse) + 1;

if polarBits == 1
for i = 0: numBits - 1
    if filteredMessage(index) >= 0
        estimatedBits(i+1) = 1;
    elseif filteredMessage(index) < 0
            estimatedBits(i+1) = -1;
    end
    index = index + samplesPerBit;
end
end

%Screws up guesses by one position if the first bit generated was actually
%a zero since it picks up a 1.  Possibly due to where the index was
%initialized
if polarBits == 0
    for i = 0: numBits - 1
    if filteredMessage(index) >= .5*samplesPerBit
        estimatedBits(i+1) = 1;
    elseif filteredMessage(index) < .5*samplesPerBit
            estimatedBits(i+1) = 0;
    end
    
    index = index + samplesPerBit;
    end
end

errorRate = (sum(abs(bits - estimatedBits))/2) / numBits

errorRates = cat(2, errorRates, errorRate);

end

% fig2 = figure();
% plot(tFiltered, filteredMessage)
% hold on
% scatter(0:19, samplesPerBit * bits)
% scatter(0:19, samplesPerBit * estimatedBits)
% xlabel('Time (sec)')
% ylabel('Signal (V)')
% title('Filtered Message and Estimated Bits')



errorRateTrial = cat(1, errorRateTrial, errorRates);

end

subplot(2,3,1)
plot(rcroMessage);
subplot(2,3,2)
plot(basebandMessage);
subplot(2,3,3)
plot(noisyMessage);
subplot(2,3,4)
plot(demodMessage);
subplot(2,3,5)
plot(filteredMessage);

avgErrorRate = mean(errorRateTrial);

fig3 = figure();

SNRTheoryDB = minDB: noiseStepDB : maxDB;
plot(SNRTheoryDB, mean(errorRateTrial))
xlabel('Eb/N0 (dB)')
ylabel('Probability of Error')
title('Pe vs. Eb/No')
